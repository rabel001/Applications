((global, doc) => {
	var handleImportFile = (evt) => {
		var file = evt.target.files[0];
		if(file === undefined) return;
		if(!/text\/plain/.test(file.type) && file.type !== '') {
			swal('File Error', `Wrong file type: '${file.type}'\nFile type must be: 'text/plain'`, 'error')
			return;
		}
		var reader = new FileReader();
		reader.onload = (event) => {
			var iFile = event.target.result;
			try {
				var nFile = atob(iFile);
				if(nFile.indexOf(':') < 10) {
					nFile = nFile.substring(nFile.indexOf(':') + 1);
				} else {
					nFile = nFile;
				}
				var publicKey = sessionStorage.getItem('publicKey');
				publicKey = atob(publicKey);
				var privateKey = sessionStorage.getItem('privateKey');
				privateKey = atob(privateKey);
				var privKeyObj = openpgp.key.readArmored(privateKey).keys[0];
				var r = sessionStorage.getItem('tgp');
				var c = JSON.parse(r);
				var o = atob((c.endplan.substring(0, c.length)).split('').reverse().join(''));
				var tpg = o.split('').reverse().join('');
				privKeyObj.decrypt(tpg).then((pbool) => {
					var options = {
						message: openpgp.message.readArmored(nFile),
						publicKeys: openpgp.key.readArmored(publicKey).keys,
						privateKeys: [privKeyObj]
					};
					openpgp.decrypt(options).then((plaintext) => {
						doc.getElementById('_txtarea').value = plaintext.data;
						swal('Decryption', 'File Decrypted', 'success');
						doc.getElementById('enc-btn').classList.remove('o-hidden');
						doc.getElementById('dec-btn').classList.add('o-hidden');
						doc.getElementById('imp-btn').classList.remove('o-hidden');
						doc.getElementById('exp-btn').classList.add('o-hidden');
					}, (error) => {
						swal('ERROR', error.message, 'error').then((next) => {
							doc.getElementById('_txtarea').value = iFile;
							swal('File', 'File was added as plain text', 'warning');
							doc.getElementById('enc-btn').classList.remove('o-hidden');
							doc.getElementById('dec-btn').classList.add('o-hidden');
							doc.getElementById('imp-btn').classList.remove('o-hidden');
							doc.getElementById('exp-btn').classList.add('o-hidden');
						});
					});
				});
			} catch(ex) {
				doc.getElementById('_txtarea').value = iFile;
				swal('File Import', 'File imported as plain text', 'warning');
				doc.getElementById('enc-btn').classList.remove('o-hidden');
				doc.getElementById('dec-btn').classList.add('o-hidden');
				doc.getElementById('imp-btn').classList.remove('o-hidden');
				doc.getElementById('exp-btn').classList.add('o-hidden');
			}
		}
		reader.onerror = (evt) => {
			swal('File error', evt.target.error.name, 'error');
			reader.abort();
		};
		reader.readAsText(file);
	}

	var handleImportKeys = (evt) => {
		var file = evt.target.files[0];
		if(file === undefined) return;
		if(!/text\/plain/.test(file.type) && file.type !== '') {
			swal('File Error', `Wrong file type: '${file.type}'\nFile type must be: 'text/plain'`, 'error')
			return;
		}
		var reader = new FileReader();
		reader.onload = (event) => {
			var iFile = event.target.result;
			var publines, privlines;
			var publicKey = /-----BEGIN\sPGP\sPUBLIC\sKEY\sBLOCK-----[\s\S]*-----END\sPGP\sPUBLIC\sKEY\sBLOCK-----/m.exec(iFile);
			var privateKey = /-----BEGIN\sPGP\sPRIVATE\sKEY\sBLOCK-----[\s\S]*-----END\sPGP\sPRIVATE\sKEY\sBLOCK-----/m.exec(iFile);
			try {
				var allPublicLines = publicKey[0].split(/\r\n|\n/);
				var allPrivateLines = privateKey[0].split(/\r\n|\n/);
			} catch(err) {
				swal('File Error', 'Files are not in the correct format.', 'error');
				reader.abort();
				return;
			}
			allPublicLines.map((line) => {
				//publines += line + '\n';
				publines = (publines == null) ? `${line}\n` : publines + `${line}\n`;
			});
			allPrivateLines.map((line) => {
				//privlines += line + '\n';
				privlines = (privlines == null) ? `${line}\n` : privlines + `${line}\n`;
			});
			sessionStorage.setItem('publicKey', btoa(publines));
			sessionStorage.setItem('privateKey', btoa(privlines));
			sessionStorage.setItem('pubprivSet', true);
			sessionStorage.setItem('firstSet', true);
			location.reload();
		}
		reader.onerror = (evt) => {
			swal('File error', evt.target.error.name, 'error');
			reader.abort();
		};
		reader.readAsText(file);
	}
	var settgp = (tgp) => {
		/////////////////////////////
		/////  setTheGamePlan   /////
		/////////////////////////////
		///// Set The Game Plan /////
		/////////////////////////////
		if(tgp == '') return;
		var y = `h@Tbgb$@mPFW4auKn&cMyXsu@tjvVS%4ppJUTyVXRpvU863Jv2G7d7t0HXpw*@GkgyNu0*Db%5Wn1AE8%z912UDPQ7GX6NGc`;
		y = btoa(y);
		var preplan = btoa(tgp.split('').reverse().join('')).split('').reverse().join('');
		var midplan = preplan + y;
		var rev = {
			"length": preplan.length,
			"endplan": midplan
		}
		rev = JSON.stringify(rev);
		sessionStorage.setItem('tgp', rev);
	}

	var testPwd = () => {
		return new Promise((resolve) => {
			var pwd = document.getElementById('login-password').value;
			settgp(pwd);
			var privateKey = sessionStorage.getItem('privateKey');
			var privkey = atob(privateKey);
			var oPrivKey = openpgp.key.readArmored(privkey).keys[0];
			oPrivKey.decrypt(pwd).then((verifyagain) => {
				resolve(true);
			}).catch((err) => {
				resolve(false);
			});
		});
	}

	var loginToAccount = () => {
		testPwd().then((next) => {
			if(next) {
				doc.querySelector('#submit').classList.add('o-hidden');
				doc.querySelector('#textarea').classList.remove('o-hidden');
				doc.querySelector('#exp-keys').classList.remove('o-hidden');
			} else {
				swal('ERROR', 'Incorrect Password', 'error');
			}
		});
	}

	var loginEnterKey = (e) => {
		if(e.target.id === "login-password") {
			if((e.keyCode || e.which) == 13) {
				doc.getElementById('login-account').click();
			}
		}
		if(e.target.id === "new-password-repeat") {
			if((e.keyCode || e.which) == 13) {
				doc.getElementById('setup-account').click();
			}
		}
	}

	var generateKeyPair = () => {
		var newEmail = doc.getElementById("new-email").value;
		var newPassword = doc.getElementById("new-password").value;
		var newPasswordRepeat = doc.getElementById("new-password-repeat").value;
		if(newPassword !== "" && newPasswordRepeat !== "") {
			if(newEmail !== "") {
				if(!/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(newEmail)) {
					swal('Email', 'Incorrect Format', 'error');
					return;
				}
				if(newPassword === newPasswordRepeat) {
					doc.getElementById("setup-account").innerHTML = "Generating keys";
					doc.getElementById("setup-account").disabled = true;
					var cycle = setInterval(() => {
						swal('Generating Openpgp Keys', 'Please wait...', 'warning', {
							buttons: false
						})
					}, 1000);
					var options = {
						userIds: [{
							name: 'Openpgpjs Encryption user',
							email: newEmail
						}],
						numBits: 4096,
						passphrase: newPassword
					};
					openpgp.generateKey(options).then((key) => {
						var pubkey = key.publicKeyArmored;
						sessionStorage.setItem('publicKey', btoa(pubkey));
						var privkey = key.privateKeyArmored;
						sessionStorage.setItem('privateKey', btoa(privkey));
						sessionStorage.setItem('pubprivSet', true);
						sessionStorage.setItem('firstSet', true);
						location.reload();
					}, (error) => {
						clearInterval(cycle);
						swal('ERROR', error.message, 'error');
						doc.getElementById('setup-account').disabled = false;
						doc.getElementById('setup-account').value = "Confirm Password";
					})
				} else {
					swal('Password Error', 'Passwords do not match.', 'error');
				}
			} else {
				swal('Email Error', 'Email address is empty.', 'error');
			}
		} else {
			swal('Password Error', 'Password cannot be null', 'error');
		}
	}

	var etext = () => {
		var text = doc.getElementById('_txtarea').value;
		if(text !== "") {
			var publicKey = sessionStorage.getItem('publicKey');
			publicKey = atob(publicKey);
			var option = {
				data: text,
				publicKeys: openpgp.key.readArmored(publicKey).keys
			}
			openpgp.encrypt(option).then((ciphertext) => {
				doc.getElementById('_txtarea').value = ciphertext.data;
				doc.getElementById('enc-btn').classList.add('o-hidden');
				doc.getElementById('dec-btn').classList.remove('o-hidden');
				doc.getElementById('imp-btn').classList.add('o-hidden');
				doc.getElementById('exp-btn').classList.remove('o-hidden');
			});
		}
	}

	var dtext = () => {
		var text = doc.getElementById('_txtarea').value;
		var privateKey = sessionStorage.getItem('privateKey');
		privateKey = atob(privateKey);
		var publicKey = sessionStorage.getItem('publicKey');
		publicKey = atob(publicKey);
		var privKeyObj = openpgp.key.readArmored(privateKey).keys[0];
		var r = sessionStorage.getItem('tgp');
		var c = JSON.parse(r);
		var o = atob((c.endplan.substring(0, c.length)).split('').reverse().join(''));
		var tgp = o.split('').reverse().join('');
		privKeyObj.decrypt(tgp).then((pbool) => {
			var options = {
				message: openpgp.message.readArmored(text),
				publicKeys: openpgp.key.readArmored(publicKey).keys,
				privateKeys: [privKeyObj]
			};
			openpgp.decrypt(options).then((plaintext) => {
				doc.getElementById('_txtarea').value = plaintext.data;
				doc.getElementById('enc-btn').classList.remove('o-hidden');
				doc.getElementById('dec-btn').classList.add('o-hidden');
				doc.getElementById('imp-btn').classList.remove('o-hidden');
				doc.getElementById('exp-btn').classList.add('o-hidden');
			}, (error) => {
				swal('ERROR', error.message, 'error');
			});
		});
	}

	var importFile = () => {
		doc.getElementById('opgp-file').click();
	}

	var exportKeys = () => {
		var publicKey = sessionStorage.getItem('publicKey');
		var publicKey = atob(publicKey);
		var privateKey = sessionStorage.getItem('privateKey');
		var privateKey = atob(privateKey);
		var data = publicKey + '\n' + privateKey + '\n'
		var blob = new Blob([data], {
			type: "text/plain;charset=UTF-8"
		});
		var url = window.URL.createObjectURL(blob);
		var a = document.createElement('a');
		a.href = url;
		a.target = '_blank';
		a.download = "Openpgp_Keys.txt";
		a.id = "CombinedKey";
		document.body.appendChild(a);
		a.click();
	}

	var exportText = () => {
		var text = doc.getElementById('_txtarea').value;
		var random = Math.random().toString(36).substr(7);
		var value = random + ':' + text
		var data = btoa(value);
		var blob = new Blob([data], {
			type: "text/plain;charset=UTF-8"
		});
		var url = global.URL.createObjectURL(blob);
		var a = doc.createElement('a');
		a.href = url;
		a.target = '_blank';
		/*swal('File name:', {
			content: {
				element: "input",
				attributes: {
				  placeholder:  "Openpgpjs_EncText.txt"
				}
			},
			buttons: {
				confirm: true,
				Cancel: {
					text: "Cancel",
					value: "cancelled",
					visible: true,
					closeModal: true,
				}
			  }
		}).then((result) => {
			if(result != null && result !== "cancelled") {
				if(result === "") {
					result = "Openpgpjs_EncText.txt";
				}
				a.download = result;
				a.id = "EncText";
				document.body.appendChild(a);
				a.click();
			}
		});*/
		a.download = "Openpgpjs_EncText.txt";
		a.id = "EncText";
		document.body.appendChild(a);
		a.click();
	}

	var clearOPGPsession = () => {
		sessionStorage.removeItem('firstSet');
		sessionStorage.removeItem('pubprivSet');
		sessionStorage.removeItem('publicKey');
		sessionStorage.removeItem('privateKey');
		sessionStorage.removeItem('tgp');
		location.reload();
	}
	doc.addEventListener('DOMContentLoaded', () => {
		doc.querySelector('#b-importkey').addEventListener('click', () => {
			doc.querySelector('#opgp-keys').click();
		});
		doc.querySelector('#opgp-keys').addEventListener('change', handleImportKeys, false);
		doc.querySelector('#opgp-file').addEventListener('change', handleImportFile, false);
		doc.querySelector('#login-account').addEventListener('click', loginToAccount);
		doc.querySelector('#login-password').addEventListener('keyup', loginEnterKey);
		doc.querySelector('#new-password-repeat').addEventListener('keyup', loginEnterKey);
		doc.querySelector('#setup-account').addEventListener("click", generateKeyPair);
		doc.querySelector('#enc-btn').addEventListener('click', etext);
		doc.querySelector('#dec-btn').addEventListener('click', dtext);
		doc.querySelector('#imp-btn').addEventListener('click', importFile);
		doc.querySelector('#exp-btn').addEventListener('click', exportText);
		doc.querySelector('#exp-keys').addEventListener('click', exportKeys);
		doc.querySelector('#cls-session').addEventListener('click', clearOPGPsession);
		doc.querySelector('#_txtarea').addEventListener('keyup', function(e) {
			if(doc.querySelector('#_txtarea').value === "") {
				doc.getElementById('enc-btn').classList.remove('o-hidden');
				doc.getElementById('dec-btn').classList.add('o-hidden');
				doc.getElementById('imp-btn').classList.remove('o-hidden');
				doc.getElementById('exp-btn').classList.add('o-hidden');
			}
		});
		doc.querySelector('#_txtarea').addEventListener('paste', function(evt) {
			var data;
			//evt.preventDefault();
			if(window.clipboardData) {
				// IE
				data = window.clipboardData.getData('Text');
			} else {
				// Standard-compliant browsers
				data = evt.clipboardData.getData('text');
			}
			//evt.target.value += data;
			if(/^-----BEGIN\sPGP\sMESSAGE-----[\s\S]*-----END\sPGP\sMESSAGE-----/m.test(data)) {
				doc.getElementById('enc-btn').classList.add('o-hidden');
				doc.getElementById('dec-btn').classList.remove('o-hidden');
				doc.getElementById('imp-btn').classList.add('o-hidden');
				doc.getElementById('exp-btn').classList.remove('o-hidden');
			} else {
				doc.getElementById('enc-btn').classList.remove('o-hidden');
				doc.getElementById('dec-btn').classList.add('o-hidden');
				doc.getElementById('imp-btn').classList.remove('o-hidden');
				doc.getElementById('exp-btn').classList.add('o-hidden');
			}
		});
		if(sessionStorage.getItem('pubprivSet') != null && sessionStorage.getItem('tgp') != null) {
			if(sessionStorage.getItem('firstSet') != null) {
				swal({
					title: 'Keys',
					text: 'Keys are loaded.',
					icon: 'success',
					timer: 5000
				});
				sessionStorage.removeItem('firstSet');
			}
			doc.querySelector('#setup').classList.add('o-hidden');
			doc.querySelector('#submit').classList.add('o-hidden');
			doc.querySelector('#textarea').classList.remove('o-hidden');
			doc.querySelector('#exp-keys').classList.remove('o-hidden');
		} else if(sessionStorage.getItem('pubprivSet') != null) {
			if(sessionStorage.getItem('firstSet') != null) {
				swal({
					title: 'Keys',
					text: 'Keys are loaded.',
					icon: 'success',
					timer: 5000
				});
				sessionStorage.removeItem('firstSet');
			}
			doc.querySelector('#setup').classList.add('o-hidden');
			doc.querySelector('#submit').classList.remove('o-hidden');
		}
	});
})(window, document)