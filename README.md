# Applications 

### <img src="https://gitlab.com/applications2/m3u8_grabber/raw/master/M3U8_Grabber/Resources/Download-Button-icon.ico"  width="20" height="20" align="middle"> [M3U8_Grabber](https://gitlab.com/applications2/m3u8_grabber)
* [![CSharp badge](https://img.shields.io/badge/Made%20with-CSharp-6a5acd.svg)](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/)
* [![DotNet badge](https://img.shields.io/badge/dotnet-v4.7.2-orange)](https://dotnet.microsoft.com/download/dotnet-framework/net472)
* Repo: https://gitlab.com/applications2/m3u8_grabber
* This utility facilitates media downloads using a m3u8 file (works with mp4 and other files).
* Wrapper for ffmpeg.exe.
----
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg?style=for-the-badge)](https://www.gnu.org/licenses/gpl-3.0)
### <img src="https://gitlab.com/rabel001/Applications/raw/master/Openpgpjs%20Encryptor/resources/app/images/favicon.png" width="20" height="20"> Openpgpjs Encryptor
* [![Electron badge](https://img.shields.io/badge/Made%20with-Electron-6a5acd.svg)](https://electronjs.org)
* [![Openpgpjs badge](https://img.shields.io/badge/Openpgpjs-v3.0.11-green.svg)](https://gitcdn.link/cdn/openpgpjs/openpgpjs/bf428b80d40ae3045ad40debe9798aeff98caa4e/dist/openpgp.js)  
* Encrypt plain text with pgp keys.

----
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg?style=for-the-badge)](https://www.gnu.org/licenses/gpl-3.0)
### <img src="https://gitlab.com/rabel001/Applications/raw/master/RSA%20Text%20Encryptor/resources/app/images/favicon.png" width="20" height="20" align="middle"> RSA Text Encryptor
* [![Electron badge](https://img.shields.io/badge/Made%20with-Electron-6a5acd.svg)](https://electronjs.org)
* [![RSA badge](https://img.shields.io/badge/jsRSA-v1.1-green.svg)](http://www-cs-students.stanford.edu/~tjw/jsbn/)  [![Generic badge](https://img.shields.io/badge/jsAES-v0.1-green.svg)](http://www-cs-students.stanford.edu/~tjw/jsbn/)
* [![Generic badge](https://img.shields.io/badge/Hash-SHA256-1abc9c.svg)](http://www-cs-students.stanford.edu/~tjw/jsbn/)
* Encrypt plain text with RSA encryption.

----
#### <img src="https://gitlab.com/rabel001/Applications/raw/master/images/M3U8_Grabber.png"  width="20" height="20" align="middle"> M3U8_Granner - Information Section
* This utility:
    * Is a wrapper for ffmpeg.exe.
    * Facilitates grabbing media files with a m3u8 file.
    * Includes a text box for command line flags, but they are not required to download media.
        * Example flags:
            * ```-user_agent "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36 OPR/65.0.3467.42"```
            * ```-headers "Referer: https://whatever.website.org"```
        * Any command line flag from the ffmpeg documentation page can be use (https://ffmpeg.org/ffmpeg.html).
    * The ffmpeg.exe executable is held in the application's resource manager.
        * Upon exectution, this application checks whether ffmpeg.exe is already in the system PATH. If so, no extraction takes place.
        * Then it checks the directory in which this compiled application is located.
        * If ffmpeg.exe is not already on the system, found in the system PATH, or in the same location of this compiled application, it is extracted  from resource manager and places in the same directory of this application.
        * To upgrade ffmpeg.exe included in this project, replace the one located in resource manager.
        * To upgrade ffmpeg.exe without changing the project or recompiling, add a system PATH to the new ffmpeg.exe or place ffmpeg.exe (overwrite) in the same location as this compiled application. 

#### <img src="https://gitlab.com/rabel001/Applications/raw/master/Openpgpjs%20Encryptor/resources/app/images/favicon.png" width="20" height="20"> Openpgpjs Encryptor - Screenshot Section
* The first screen lets you setup your pgp keys or import. Once this is done, you can encrypt text and decrypt text that was ecrypted with the keys that have been applied. Once the keys are setup, you can export them.
 
<img src="https://gitlab.com/rabel001/Applications/raw/master/images/Openpgp_Setup.png" width="760" align="middle">

* Once you confirm your email and password, you will be notified that the application is generation new pgp public and private keys.

<img src="https://gitlab.com/rabel001/Applications/raw/master/images/Openpgp_KeyGeneration.png" width="760" align="middle">

* You will then be prompted to login. This sets the keys to be used for encryption.

<img src="https://gitlab.com/rabel001/Applications/raw/master/images/Openpgp_Login.png" width="760" align="middle">

* Here is where the magic happens.

<img src="https://gitlab.com/rabel001/Applications/raw/master/images/Openpgp_Workspace1.png" width="380" align="middle">&nbsp;<img src="https://gitlab.com/rabel001/Applications/raw/master/images/Openpgp_Workspace2.png" width="380" align="middle">

   - Here you can do the following.
       - Encrypt and decrypt text.
       - Import text that has been encrypted with Openpgp keys (in use or imported). 
       - Export encrypted text to decrypt later (make sure to export your keys).
       - Export Openpgp keys (to single file).
       - Clear the session to start over at the setup screen.

